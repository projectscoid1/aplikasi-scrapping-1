import random
from app.res import spiders
from datetime import datetime
from icecream import ic
from time import sleep
import traceback
import os

KEYWORD = "tobacco"
START_PAGE = 1
END_PAGE = None

blank_page = 0
run_spider = True
page = START_PAGE

try:
    print(run_spider)
    while run_spider:
        current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(page, "\n", current_time, flush=True)
        spider = spiders.SpiderBusiness1()
        suppliers = spider.get_supplier_data_by_keyword(
            keyword=KEYWORD, page=page, insert_into_db=True
        )
        print(os.getpid(), flush=True)
        status = "scrapping end"
        if blank_page >= 10:
            run_spider = False
            print(status, flush=True)
            break
        elif len(suppliers) == 0:
            blank_page += 1
            print(blank_page, flush=True)
        else:
            blank_page = 0

        if page == END_PAGE:
            run_spider = False
            print(status, flush=True)
            break
        else:
            page += 1
            if page % 10 == 0:
                seconds = random.randint(10, 20)
                status = f"sleep for {seconds} seconds"
                print(status, flush=True)
                sleep(seconds)
except:
    traceback.print_exc()

print(run_spider, flush=True)
