import random
from app.res import spiders
from icecream import ic
from time import sleep
import traceback
import os

KEYWORD = "tobacco"
START_PAGE = 160
END_PAGE = 165

blank_page = 0
run_spider = True
page = START_PAGE

try:
    print(run_spider, flush=True)
    while run_spider:
        print(page)
        spider = spiders.SpiderBusiness1()
        products = spider.get_product_data_by_keyword(
            keyword=KEYWORD, page=page, find_supplier_url=True, insert_into_db=True
        )

        print(os.getpid(), flush=True)
        status = "scrapping end"
        if blank_page >= 2:
            run_spider = False
            print(status, flush=True)
            break
        elif len(products) == 0:
            blank_page += 1
            print(blank_page, flush=True)
        else:
            blank_page = 0

        if page == END_PAGE:
            run_spider = False
            print(status, flush=True)
            break
        else:
            page += 1
            seconds = random.randint(10, 20)
            status = f"sleep for {seconds} seconds"
            print(status, flush=True)
            sleep(seconds)
except:
    traceback.print_exc()

print(run_spider, flush=True)
