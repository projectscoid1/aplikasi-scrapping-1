import asyncio
import traceback
from abc import ABC, abstractmethod
from enum import Enum
from typing import List, Tuple

import aiohttp
from bs4 import BeautifulSoup
import requests
from app.res import parser
from datetime import datetime
from time import sleep
import random

class Category(Enum):
    SUPPLIER = "supplier"
    PRODUCT = "product"
    NEWS = "news"
    EVENT = "event"
    ARTICLE = "article"
    JOB = "job"


class Spider(ABC):
    parser_depth_0 = parser.ParserDepth0

    @property
    def headers(self):
        return {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.5",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Cookie": "AWSALB=AAIZAh5IBbiWjfh2mizv8ywFIsEaAsV09zROZ99ig1jUIzHO8e6PQ8/ARCgA+9o5utBX9O4s8zpD+iSKX986KVy6EN/OYEoQw9Q5l4NfsGPE8V3WMD4SnS3OJPXI; AWSALBCORS=AAIZAh5IBbiWjfh2mizv8ywFIsEaAsV09zROZ99ig1jUIzHO8e6PQ8/ARCgA+9o5utBX9O4s8zpD+iSKX986KVy6EN/OYEoQw9Q5l4NfsGPE8V3WMD4SnS3OJPXI",
            "Host": "www.business1.com",
            "Pragma": "no-cache",
            "Sec-Fetch-Dest": "document",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "none",
            "Sec-Fetch-User": "?1",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0",
        }

    @property
    @abstractmethod
    def _base_url(self):
        ...

    @property
    def _parser(self):
        return "html.parser"

    def check_status(self) -> Tuple[int, str]:
        status_code = requests.get(self._base_url).status_code
        if status_code == 200:
            message = f"{self._base_url} OK"
        else:
            message = f"Error when connecting to {self._base_url}"
        return status_code, message

    def search_keyword(self, keyword: str) -> parser.BeautifulSoup:
        """{self.base_url}/search/results?query={keyword}"""
        return parser.BeautifulSoup(
            self._sync_get(
                url=f"{self._base_url}/search/results", params={"query": keyword}
            ),
            self._parser,
        )

    def nr_of_keyword_found(self, keyword: str) -> dict:
        return self._nr_of_keyword_found(html_soup=self.search_keyword(keyword=keyword))

    def search_keyword_by_category_and_page(
        self, keyword: str, category: Category, page: int = 1
    ) -> parser.BeautifulSoup:
        """{self.base_url}/search-results?query={keyword}&type={category}&page={page}"""
        return parser.BeautifulSoup(
            self._sync_get(
                url=f"{self._base_url}/search-results",
                params={"query": keyword, "type": category.value, "page": page},
            ),
            self._parser,
        )

    async def set_product_supplier_url(self, products: List[parser.Product]):
        product_pages = await self._async_get(urls=[product.url for product in products])
        parser.ParserDepth0._find_and_set_product_supplier_url(
            products=products,
            product_pages=product_pages
        )

    def get_product_data_by_keyword(
        self,
        keyword: str,
        page: int = 1,
        find_supplier_url: bool = False,
        insert_into_db: bool = False,
    ) -> List[parser.Product]:
        """
        Steps:
        1) Scraping {self.base_url}/search-results?query={keyword}&type=product&page={page}
        2) Parsing the result page
        3) For each product found in the page, return as parser.Product. Usually 10 products in 1 page.
        Optional:
        - Find the supplier url by scraping product overview page,
           e.g. https://www.tobacco1.com/tobacco-products/pipe-tobacco
        - Insert each of the product into database
        """
        products = parser.ParserDepth0.get_product_data_by_keyword(
            html_soup=self.search_keyword_by_category_and_page(
                keyword=keyword, category=Category("product"), page=page
            )
        )
        if find_supplier_url:
            asyncio.run(self.set_product_supplier_url(products=products))
        if insert_into_db:
            [
                product.insert_into_table()
                for product in products
                if isinstance(product, parser.Product)
            ]
        return products

    def get_supplier_data_by_keyword(
        self,
        keyword: str,
        page: int = 1,
        insert_into_db: bool = False,
    ) -> List[parser.Supplier]:
        """
        Steps:
        1) Scraping {self.base_url}/search-results?query={keyword}&type=supplier&page={page}
        2) Parsing the result page
        3) For each supplier found in the page, return as parser.Supplier. Usually 10 products in 1 page.
        Optional:
        - Insert each of the product into database
        """
        suppliers = parser.ParserDepth0.get_supplier_data_by_keyword(
            html_soup=self.search_keyword_by_category_and_page(
                keyword=keyword, category=Category("supplier"), page=page
            )
        )
        if insert_into_db:
            [
                supplier.insert_into_table()
                for supplier in suppliers
                if isinstance(supplier, parser.Supplier)
            ]
        return suppliers

    def get_supplier_url_in_search_page(
        self, keyword: str, insert_into_db: bool = False
    ) -> List[parser.Supplier]:
        """
        Steps:
        1) Scraping {self.base_url}/search/results?query={keyword}
        2) Parsing the result page, get the data in supplier section
        3) For each supplier found in the section, return as parser.Supplier. Usually 10 products in 1 page.
        Optional:
        - Insert each of the product into database
        """
        suppliers = parser.ParserDepth0.get_supplier_url_in_search_page(
            html_soup=self.search_keyword(keyword=keyword)
        )
        if insert_into_db & (not (suppliers is None)):
            [
                supplier.insert_into_table()
                for supplier in suppliers
                if isinstance(supplier, parser.Supplier)
            ]
        return suppliers

    async def get_supplier_data(
        self, urls: List[str], insert_into_db: bool = False
    ) -> List[parser.Supplier]:
        supplier_data_container = []
        try:
            supplier_data_container = parser.ParserDepth1.get_supplier_data(
                await self._get_supplier_raw_data(urls=urls)
            )
            if insert_into_db:
                [
                    data.insert_into_table()
                    for data in supplier_data_container
                    if isinstance(data, parser.Supplier)
                ]
        except:
            traceback.print_exc()
        finally:
            return supplier_data_container

    async def _get_supplier_raw_data(
        self, urls: List[str]
    ) -> List[parser.BeautifulSoup]:
        html_soup_container = []
        try:
            supplier_pages = await self._async_get(urls=urls)
            html_soup_container = [
                BeautifulSoup(page, "html.parser") for page in supplier_pages
            ]
        except:
            traceback.print_exc()
        finally:
            return html_soup_container

    @staticmethod
    def find_proxy_from_getproxylist() -> List[dict]:
        url = "https://api.getproxylist.com/proxy?"
        params = dict(
            protocol="http",
            allowsUserAgentHeader=True,
            allowsCustomHeaders=True,
            allowsHttps=True,
            minUptime=99,
            maxConnectTime=10,
        )
        r = requests.get(url, params=params)
        r.raise_for_status()

        res = r.json()
        return [
            {
                "http": f"""http://{res.get("ip")}:{res.get("port")}""",
                "https": f"""https://{res.get("ip")}:{res.get("port")}""",
            }
        ]

    @staticmethod
    def find_proxy_from_pubproxy() -> List[dict]:
        url = "http://pubproxy.com/api/proxy?format=json&type=http&last_check=5&https=true&user_agent=true&speed=30&limit=5&level=elite"
        r = requests.get(url)
        r.raise_for_status()
        res = r.json()
        return [
            {
                "http": f"""http://{ip.get("ipPort")}""",
                "https": f"""https://{ip.get("ipPort")}""",
            }
            for ip in res
        ]

    def _sync_get(self, url: str, params: dict):
        resp = ""
        try:
            r = requests.get(url, params=params, timeout=15)
            print(r.url)
            r.raise_for_status()
            resp = r.text
            # print(resp)
        except (requests.exceptions.HTTPError, requests.exceptions.Timeout):
            status = f"pause scraper for 5 minutes from {datetime.now()}"
            print(status)
            sleep(5 * 60)
        except (
            requests.exceptions.ConnectTimeout,
            requests.exceptions.ProxyError,
            requests.exceptions.SSLError,
        ) as e:
            status = f"proxy error: {e}, searching another proxy"
            print(status)
            return self._sync_get(url=url, params=params)
        except:
            traceback.print_exc()
        finally:
            return resp

    @staticmethod
    async def __fetch(session: aiohttp.ClientSession, url: str):
        result = ""
        try:
            print(url)
            if not (isinstance(url, str)):
                raise ValueError("URL invalid")
            async with session.get(url=url) as resp:
                result = await resp.text()
        except:
            traceback.print_exc()
        finally:
            return result

    async def _async_get(self, urls: List[str], max_batch_size:int=5):
        result_container = []
        for i in range(0, len(urls), max_batch_size):
            tasks = []
            async with aiohttp.ClientSession() as session:
                [tasks.append(Spider.__fetch(session=session, url=url)) for url in urls[i : i + max_batch_size]]
                result_container.extend(await asyncio.gather(*tasks, return_exceptions=True))
                sleep(random.randint(15,30))
                print("sleep for 10 seconds...")
        return result_container


class SpiderBusiness1(Spider):
    @property
    def _base_url(self) -> str:
        return "https://www.business1.com"


class SpiderTobacco1(Spider):
    @property
    def _base_url(self) -> str:
        return "https://www.tobacco1.com"
