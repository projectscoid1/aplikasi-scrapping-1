from enum import Enum
import traceback
import aiohttp
from bs4 import BeautifulSoup, Tag
from dataclasses import dataclass, field
from typing import List, Union
from app.res import tables as tbl


@dataclass
class Product:
    id: int
    id_supplier: int
    name: str
    url: str
    url_supplier: str = field(default=None)

    @classmethod
    def from_search_page(cls, tag: Tag):
        product = None
        try:
            _attribute = tag.attrs
            _title = tag.find("h3")
            product = Product(
                id=_attribute.get("data-product"),
                id_supplier=_attribute.get("data-id"),
                name=None if _title is None else _title.text.strip(),
                url=_attribute.get("data-anchor"),
            )
        except:
            traceback.print_exc()
        finally:
            return product

    def _check_data_validity(self):
        return all(
            [
                str(self.id).isdigit(),
                isinstance(self.name, str),
                isinstance(self.url, str),
            ]
        )

    def insert_into_table(self):
        if self._check_data_validity():
            q = (
                tbl.product.insert()
                .prefix_with("OR REPLACE")
                .values(
                    id=self.id,
                    id_supplier=self.id_supplier,
                    name=self.name,
                    url=self.url,
                    url_supplier=self.url_supplier,
                )
            )
            with tbl.engine.connect() as conn:
                res = conn.execute(q)

            return res.lastrowid
        return -1

    def to_dict(self):
        return {
            "id": self.id,
            "id_supplier": self.id_supplier,
            "name": self.name,
            "url": self.url,
            "url": self.url_supplier,
        }


@dataclass
class Supplier:
    id: int
    name: str
    url: str
    telephone: str
    address: str
    zip_code: str
    city_region: str
    country: str
    email: str
    website: str
    fax: str
    vat_number: str
    chamber_of_commerce: str

    @classmethod
    def from_supplier_page(cls, html_soup: BeautifulSoup):
        supplier = None
        try:
            id = html_soup.find("ul").attrs.get("data-company", "")
            if not (id.isdigit()):
                return
            url = html_soup.find("meta", attrs={"property": "og:url"}).attrs.get(
                "content"
            )
            keys_list = html_soup.find_all("div", attrs={"class": "widget-inner"})
            values_list = html_soup.find_all("div", attrs={"class": "widget-inner"})

            contact_info_index = None
            for index, keys in enumerate(keys_list):
                if len(keys.find_all("div", attrs={"class": "collection-name"})) > 0:
                    contact_info_index = index
                    break
            if not (contact_info_index is None):
                keys = keys_list[contact_info_index].find_all(
                    "div", attrs={"class": "collection-name"}
                )
                values = values_list[contact_info_index].find_all(
                    "div", attrs={"class": "collection-value"}
                )
            else:
                keys = []
                values = []

            data = dict(
                zip(
                    [key.text.replace(":", "").strip() for key in keys],
                    [value.text.strip() for value in values],
                )
            )
            supplier = Supplier(
                id=int(id),
                name=data.get("Company Name"),
                url=url,
                telephone=data.get("Telephone"),
                address=data.get("Address"),
                zip_code=data.get("Zip/Postal code"),
                city_region=data.get("City/Region"),
                country=data.get("Country"),
                email=data.get("Email"),
                website=data.get("Website"),
                fax=data.get("Fax"),
                vat_number=data.get("Vat number"),
                chamber_of_commerce=data.get("Chamber of Commerce"),
            )
        except:
            traceback.print_exc()
        finally:
            return supplier

    @classmethod
    def from_search_page(cls, tag: Tag):
        supplier = None
        try:
            _attribute = tag.attrs
            name = _attribute.get("data-name")
            url = _attribute.get("data-anchor")
            supplier = Supplier(
                id=_attribute.get("data-id", ""),
                name=name.strip() if isinstance(name, str) else None,
                url="find in search page"
                if not (url.strip().startswith("https"))
                else url,
                telephone=None,
                address=None,
                zip_code=None,
                city_region=None,
                country=None,
                email=None,
                website=None,
                fax=None,
                vat_number=None,
                chamber_of_commerce=None,
            )
        except:
            traceback.print_exc()
        finally:
            return supplier

    def _check_data_validity(self):
        return all(
            [
                str(self.id).isdigit(),
                isinstance(self.name, str),
                isinstance(self.url, str),
            ]
        )

    def insert_into_table(self):
        if self._check_data_validity():
            q = (
                tbl.supplier.insert()
                .prefix_with("OR REPLACE")
                .values(
                    id=self.id,
                    name=self.name,
                    url=self.url,
                    telephone=self.telephone,
                    address=self.address,
                    zip_code=self.zip_code,
                    city_region=self.city_region,
                    country=self.country,
                    email=self.email,
                    website=self.website,
                    fax=self.fax,
                    vat_number=self.vat_number,
                    chamber_of_commerce=self.chamber_of_commerce,
                )
            )
            with tbl.engine.connect() as conn:
                res = conn.execute(q)

            return res.lastrowid
        return -1


class ParserDepth0:
    @staticmethod
    def get_product_data_by_keyword(html_soup: BeautifulSoup) -> List[Product]:
        """
        parsing {self.base_url}/search-results?query={keyword}&type={category}&page={page}
        """
        return [
            Product.from_search_page(data) for data in html_soup.find_all("article")
        ]

    @staticmethod
    def get_supplier_data_by_keyword(html_soup: BeautifulSoup) -> List[Supplier]:
        """
        parsing {self.base_url}/search-results?query={keyword}&type={category}&page={page}
        """
        return [
            Supplier.from_search_page(data) for data in html_soup.find_all("article")
        ]

    @staticmethod
    def nr_of_keyword_found(html_soup: BeautifulSoup, category: Enum) -> dict:
        """parsing {self.base_url}/search/results?query={keyword}"""
        return {
            k.value: ParserDepth0._nr_of_keyword_found_by_category(html_soup, k.value)
            for k in category
        }

    @staticmethod
    def get_supplier_url_in_search_page(html_soup: BeautifulSoup) -> List[Supplier]:
        """parsing {self.base_url}/search/results?query={keyword}"""
        supplier_section = html_soup.find("section", attrs={"data-type": "supplier"})
        if supplier_section is None:
            return
        return [
            Supplier.from_search_page(data)
            for data in supplier_section.find_all("article")
        ]

    @staticmethod
    async def __fetch(session: aiohttp.ClientSession, url: str):
        """helper method for all async functions"""
        async with session.get(url) as res:
            return await res.text()

    @staticmethod
    def _set_product_supplier_url(product: Product, html_soup: BeautifulSoup):
        try:
            supplier = html_soup.find(
                "a", attrs={"title": "Return to overview"}
            ).attrs.get("href", "")
            product.url_supplier = supplier.replace("#products", "")
        except AttributeError as e:
            status = f"Supplier url not found. Product: {product.url}"
            print(status)
        except:
            traceback.print_exc()

    @staticmethod
    def _find_and_set_product_supplier_url(
        products: List[Product], product_pages: List[str]
    ):
        """helper method for self.product_data_by_keyword"""
        for index, product in enumerate(products):
            try:
                ParserDepth0._set_product_supplier_url(
                    product, BeautifulSoup(product_pages[index], "html.parser")
                )
            except:
                traceback.print_exc()

        return products

    @staticmethod
    def _nr_of_keyword_found_by_category(
        html_soup: BeautifulSoup, category: str
    ) -> int:
        """helper method for self.nr_of_keyword_found()"""
        nr = 0
        try:
            item = html_soup.find("section", attrs={"data-type": category})

            if isinstance(item, Tag):
                if len(splitted_item := item.find("h3").text.split()):
                    nr = int(splitted_item[0])
        except:
            traceback.print_exc()
        finally:
            return nr


class ParserDepth1:
    @staticmethod
    def get_supplier_data(html_soup_container: List[BeautifulSoup]) -> List[Supplier]:
        """page example: https://www.agriculture1.com/agriculture-suppliers/captainexport"""
        res = []
        for html_soup in html_soup_container:
            res.append(Supplier.from_supplier_page(html_soup=html_soup))
        return res
