import sqlalchemy as sql
import os
from sqlalchemy.sql.schema import MetaData
from sqlalchemy_utils import database_exists, create_database
from sys import argv

KEYWORD = input("Masukkan keyword: ")
os.environ["KEYWORD"] = KEYWORD
db_uri = f"sqlite:///{KEYWORD}.sqlite3"

opts = [opt for opt in argv[1:] if opt.startswith("--")]
is_debugging = "--debug" in opts
print(is_debugging)
engine = sql.create_engine(db_uri, echo=is_debugging)

meta = MetaData()

supplier = sql.Table(
    "supplier",
    meta,
    sql.Column("id", sql.INTEGER, primary_key=True, unique=True),
    sql.Column("name", sql.VARCHAR(256)),
    sql.Column("url", sql.TEXT),
    sql.Column("telephone", sql.VARCHAR(32)),
    sql.Column("address", sql.VARCHAR(256)),
    sql.Column("zip_code", sql.VARCHAR(16)),
    sql.Column("city_region", sql.VARCHAR(256)),
    sql.Column("country", sql.VARCHAR(128)),
    sql.Column("email", sql.VARCHAR(256)),
    sql.Column("website", sql.VARCHAR(256)),
    sql.Column("fax", sql.VARCHAR(256)),
    sql.Column("vat_number", sql.VARCHAR(256)),
    sql.Column("chamber_of_commerce", sql.VARCHAR(256)),
)
product = sql.Table(
    "product",
    meta,
    sql.Column("id", sql.INTEGER, primary_key=True, unique=True),
    sql.Column("id_supplier", sql.INTEGER),
    sql.Column("name", sql.VARCHAR(256)),
    sql.Column("url", sql.TEXT),
    sql.Column("url_supplier", sql.TEXT),
)


def create_if_not_exists():
    if not (database_exists(engine.url)):
        create_database(engine.url)
        meta.create_all(engine)
