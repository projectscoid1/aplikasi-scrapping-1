from app.res.spiders import Spider, SpiderBusiness1, SpiderTobacco1, Category
from app.res.parser import ParserDepth0
from flask import Blueprint, jsonify, request
from functools import wraps

spiders_app = Blueprint("spiders_app", __name__)
spiders = {"business1": SpiderBusiness1, "tobacco1": SpiderTobacco1}


def get_website_spider(website: str) -> Spider:
    spider = spiders.get(website)
    if not (spider is None):
        return spider()


def bad_request_400(message: str):
    status_code = 400
    return jsonify({"status": status_code, "message": message}), status_code


def find_spider(func):
    @wraps(func)
    def is_spider_found(website: str, **kwargs):
        if spiders.get(website) is None:
            return bad_request_400(message="Spider not found")
        return func(website, **kwargs)

    return is_spider_found


@spiders_app.route("/api/v1/<website>/checkConnection/", methods=["GET"])
@find_spider
def check_connection(website):
    status_code, message = get_website_spider(website).check_status()

    return jsonify({"status": status_code, "message": message}), status_code


@spiders_app.route("/api/v1/<website>/findNumberOfKeyword", methods=["GET"])
@find_spider
def find_nr_of_keyword(website):
    if len(keyword := request.args.get("keyword", "")) < 2:
        return bad_request_400(message="Keyword must be 2 or more characters")

    return (
        jsonify(
            ParserDepth0.nr_of_keyword_found(
                html_soup=get_website_spider(website).search_keyword(keyword),
                category=Category,
            )
        ),
        200,
    )


@spiders_app.route("/api/v1/<website>/products/<page>", methods=["GET", "POST"])
@find_spider
def products(website, page):
    if len(keyword := request.args.get("keyword", "")) < 2:
        return bad_request_400(message="Keyword must be 2 or more characters")

    if request.method == "POST":
        products_obj = get_website_spider(website).get_product_data_by_keyword(
            keyword=keyword, page=int(page), find_supplier_url=True, insert_into_db=True
        )
        products = [product.to_dict() for product in products_obj]
        status_code = 201
    elif request.method == "GET":
        products = {"status": "under development"}
        status_code = 200

    return jsonify(products), status_code
