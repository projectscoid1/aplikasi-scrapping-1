from app.res.tables import create_if_not_exists

create_if_not_exists()

from flask import Flask


app = Flask(__name__)


@app.route("/", methods=["GET"])
def home():
    return "Welcome to www.business1.com Scraper App"


from app.res.routes import spiders_app

app.register_blueprint(spiders_app)
