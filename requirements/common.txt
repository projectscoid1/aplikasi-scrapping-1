aiohttp==3.8.0
beautifulsoup4==4.10.0
flask==2.0.2
requests==2.26.0
sqlalchemy==1.4.27
sqlalchemy-utils==0.37.9