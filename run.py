import asyncio
import random
import time
import traceback
import sqlalchemy as sql
import os

from datetime import datetime

from app.res import spiders
from app.res import tables as tbl


def get_product_data_by_keyword(keyword: str):
    start_status = "get supplier data by keyword"
    print(f"start_status: {start_status}", flush=True)
    run_spider = True
    page = 1
    blank_page = 0
    try:
        print(f"run_spider: {run_spider}", flush=True)
        while run_spider:
            current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(f"page: {page}, current_time: {current_time}", flush=True)
            spider = spiders.SpiderBusiness1()
            suppliers = spider.get_product_data_by_keyword(
                keyword=keyword, page=page, insert_into_db=True, find_supplier_url=True
            )
            if blank_page >= 10:
                run_spider = False
                break
            elif len(suppliers) == 0:
                blank_page += 1
                print(f"blank_page: {blank_page}", flush=True)
            else:
                blank_page = 0

            page += 1
            if page % 5 == 0:
                sleep_time = random.randint(15, 30)
                status = f"sleep for {sleep_time} seconds"
                print(f"status: {status}", flush=True)
                time.sleep(sleep_time)
    except:
        traceback.print_exc()
    finally:
        run_spider = False
        print(f"run_spider: {run_spider}", flush=True)


def get_supplier_data_by_keyword(keyword: str):
    start_status = "get supplier data by keyword"
    print(f"start_status: {start_status}", flush=True)
    run_spider = True
    page = 1
    blank_page = 0
    try:
        print(f"run_spider: {run_spider}", flush=True)
        while run_spider:
            current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(f"page: {page}, current_time: {current_time}", flush=True)
            spider = spiders.SpiderBusiness1()
            suppliers = spider.get_supplier_data_by_keyword(
                keyword=keyword, page=page, insert_into_db=True
            )
            if blank_page >= 10:
                run_spider = False
                break
            elif len(suppliers) == 0:
                blank_page += 1
                print(f"blank_page: {blank_page}", flush=True)
            else:
                blank_page = 0

            page += 1
            if page % 5 == 0:
                sleep_time = random.randint(15, 30)
                status = f"sleep for {sleep_time} seconds"
                print(f"status: {status}", flush=True)
                time.sleep(sleep_time)
    except:
        traceback.print_exc()
    finally:
        run_spider = False
        print(f"run_spider: {run_spider}", flush=True)


def get_supplier_url_in_search_page():
    start_status = "get supplier url in search page"
    print(f"start_status: {start_status}", flush=True)
    nr_of_supplier_searched = 0
    try:
        print(f"nr_of_supplier_searched: {nr_of_supplier_searched}", flush=True)
        with tbl.engine.connect() as conn:
            q = sql.select([tbl.supplier.c.name]).where(
                sql.and_(
                    tbl.supplier.c.url == "find in search page",
                )
            )
            res_generator = conn.execute(q).fetchall()
        for res in res_generator:
            if res is None:
                status = "No keyword found"
                print(f"status: {status}", flush=True)
                break
            else:
                keyword = res[0]
                status = f"Find keyword: {keyword}"
                print(f"status: {status}", flush=True)

                spider = spiders.SpiderBusiness1()
                spider.get_supplier_url_in_search_page(
                    keyword=keyword, insert_into_db=True
                )

                nr_of_supplier_searched += 1
                if nr_of_supplier_searched % 5 == 0:
                    sleep_time = random.randint(15, 30)
                    status = f"sleep for {sleep_time} seconds, number of keyword searched: {nr_of_supplier_searched}"
                    print(f"status: {status}", flush=True)
                    time.sleep(sleep_time)
    except:
        traceback.print_exc()


def get_data_from_supplier_page():
    try:
        with tbl.engine.connect() as conn:
            q = sql.select([tbl.supplier.c.url]).where(
                sql.and_(
                    sql.not_(tbl.supplier.c.url.like("find in search page")),
                    sql.not_(tbl.supplier.c.url.like("https://www.weapons1%")),
                    tbl.supplier.c.telephone == None,
                )
            )
            r = conn.execute(q).fetchall()
            suppliers_url = list(set(r))

        suppliers_url = [supplier[0] for supplier in suppliers_url]

        BATCH_SIZE = 5
        BATCH_NO = 1
        START_POINT = 0

        nr_supplier_found = len(suppliers_url)
        print(f"nr_supplier_found: {nr_supplier_found}", flush=True)
        for i in range(START_POINT, nr_supplier_found, BATCH_SIZE):
            print(f"batch_no: {BATCH_NO}", flush=True)
            batch = suppliers_url[i : i + BATCH_SIZE]
            spider = spiders.SpiderBusiness1()
            asyncio.run(spider.get_supplier_data(urls=batch, insert_into_db=True))
            supplier_left = max(nr_supplier_found - (i + BATCH_SIZE), 0)
            print(f"supplier_left: {supplier_left}", flush=True)
            sleep_time = random.randint(15, 30)
            print(f"sleep_time : {sleep_time}", flush=True)
            time.sleep(sleep_time)
            BATCH_NO += 1
    except:
        traceback.print_exc()

def main():
    keyword = os.environ.get("KEYWORD")
    print(f"Search keyword: {keyword}")
    get_product_data_by_keyword(keyword=keyword)
    get_supplier_data_by_keyword(keyword=keyword)
    get_supplier_url_in_search_page()
    get_data_from_supplier_page()

    with tbl.engine.connect() as conn:
        conn.execute(
            tbl.supplier.update().where(tbl.supplier.c.url == "find in search page").values(url = "error 500")
        )


if __name__ == "__main__":
    main()