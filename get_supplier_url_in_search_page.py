from app.res import tables as tbl
from app.res import spiders
from icecream import ic
import sqlalchemy as sql
import random
import traceback
from time import sleep


nr_of_keyword_search = 0
try:
    print(nr_of_keyword_search, flush=True)
    with tbl.engine.connect() as conn:
        q = (
            sql.select([tbl.supplier.c.name])
            .where(sql.and_(tbl.supplier.c.url == "find in search page", tbl.supplier.c.id > 482288))
        )
        res_generator = conn.execute(q).fetchall()
    for res in res_generator:
        if res is None:
            status = "No keyword found"
            print(status, flush=True)
            break
        else:
            keyword = res[0]
            status = f"Find keyword: {keyword}"
            print(status, flush=True)

            spider = spiders.SpiderBusiness1()
            suppliers_data = spider.get_supplier_url_in_search_page(
                keyword=keyword, insert_into_db=True
            )
            
            nr_of_keyword_search += 1
            if nr_of_keyword_search % 10 == 0:
                seconds = random.randint(10, 20)
                status = f"sleep for {seconds} seconds, number of keyword searched: {nr_of_keyword_search}"
                print(status, flush=True)
                sleep(seconds)
except:
    traceback.print_exc()
