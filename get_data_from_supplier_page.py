import asyncio
import time
import random

from app.res import spiders
from app.res.tables import engine, supplier, sql
from icecream import ic

with engine.connect() as conn:
    q = (
        sql
        .select([supplier.c.url])
        .where(
            sql.and_(
                sql.not_(supplier.c.url.like("https://www.weapons1%")),
                supplier.c.telephone == None
            )
        )
    )
    r = conn.execute(q).fetchall()
    suppliers_url = list(set(r))

suppliers_url = [supplier[0] for supplier in suppliers_url]

BATCH_SIZE = 10
BATCH_NO = 1
START_POINT = 0

nr_supplier_found = len(suppliers_url) 
print(nr_supplier_found, flush=True)
for i in range(START_POINT, nr_supplier_found, BATCH_SIZE):
    print(BATCH_NO, flush=True)
    batch = suppliers_url[i : i + BATCH_SIZE]
    spider = spiders.SpiderBusiness1()
    asyncio.run(spider.get_supplier_data(urls=batch, insert_into_db=True))
    supplier_left = max(nr_supplier_found - (i + BATCH_SIZE), 0)
    print(supplier_left, flush=True)
    sleep_time = random.randint(10, 15)
    print(sleep_time, flush=True)
    time.sleep(sleep_time)
    BATCH_NO += 1
