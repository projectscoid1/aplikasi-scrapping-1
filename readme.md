# [Membuat Program Aplikasi Scrapping URL!](https://projects.co.id/public/browse_projects/view/b91319/membuat-program-aplikasi-scrapping-url)

## Deskripsi
Project berupa membuat aplikasi simple scrapping url. Program bisa dibuat untuk input url list dan dari url tersebut bisa diatur depthnya (bisa bercabang). dalam url yang didapat mengambil data berupa email, no telp, instagram, facebook, dan linkedin. Harus diperhatikan agar tidak terkena SPAM karena sebagian url ada di 1 domain. hasil akan otomatis export dalam bentuk csv file.

## Requirements
- Python version >=3.9.7
- Program mampu men-scraping `tobacco1` dan `business1`
- Mampu menscraping berdasarkan keyword
- Mengatur *depth* scraping
- Memakai IP Proxy agar tidak dianggap spam (note: situs business1.com tidak bisa menggunakan proxy)
- Hasil scraping disimpan dalam file CSV (rekomendasi: file sqlite3)

Project Owner: [matiusryan1!](https://projects.co.id/public/browse_users/view/5bf822/matiusryan1-matiusryan1)